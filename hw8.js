var studentsAndPoints = [
    'Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0,
];

//1. Создать массив students. Заполнить его данными из studentsAndPoints. Каждый элемент массива — это объект: в поле name — имя студента, а в поле point — его балл. Также нужно добавить метод show, который выводит в консоль строку вида: "Студент Виктория Заровская набрал 30 баллов". 

var students = [];
var show = function() {
    console.log('Студент %s набрал %d баллов', this.name, this.points);
}


for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
    addStudent(studentsAndPoints[i], studentsAndPoints[i+1]);
}

console.group('Список студентов:');
students.forEach(function (student, i) {
    student.show();
});
console.groupEnd();

//2. Добавить в список студентов «Николай Фролов» и «Олег Боровой». У них пока по 0 баллов.

function addStudent(name, points){
    students.push({
      name: name, 
      points: points, 
      show: show
    });
}

addStudent('Николай Фролов', 0);
addStudent('Олег Боровой', 0);

console.group('Обновленный список студентов:');
students.forEach(function (student) {
    student.show();
});
console.groupEnd();

//3. Увеличить баллы студентам «Ирина Овчинникова» и «Александр Малов» на 30, а Николаю Фролову — на 10.

function addPoints(name, add) {
    var student = students.find(function (elem) { 
        return elem.name == name;
    });
    student.points += add;
}

addPoints('Ирина Овчинникова', 30);
addPoints('Александр Малов', 30);
addPoints('Николай Фролов', 10);

console.group('Обновлены баллы:');
students.forEach(function (student) {
    student.show();
});
console.groupEnd();

//4. Вывести список студентов, набравших 30 и более баллов, без использования циклов.
7
console.group('Студенты, набравшие 30 и более баллов:');
students.forEach(function (student){
    if (student.points >= 30) {
        console.log('Студент %s набрал %d баллов', student.name, student.points);
    }
});
console.groupEnd();

//5. Учитывая, что каждая сделанная работа оценивается в 10 баллов, добавить всем студентам поле worksAmount равное кол-ву сделанных работ.

students.forEach(function (student) {
    student.worksAmount = student.points / 10;
});

console.group('Количество выполненных работ:');
students.forEach(function (student) {
    console.log(student.name + ' — ' + student.worksAmount);
});
console.groupEnd();

//6. Добавить объекту students метод findByName, который принимает на вход имя студента и возвращает соответствующий объект, либо undefined.

students.findByName = function (name) {
    return this.find(function (elem) {
        return elem.name == name;
    });
}

console.log(students.findByName('Алексей Левенец'));// Объект.
students.findByName('Глеб Стукалов').show();// с использованием метода show.